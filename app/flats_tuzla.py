#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from logging.handlers import TimedRotatingFileHandler
from datetime import datetime
import time
import app_helpers
import configTz as config
from pikscraper import PikScraper, DesiredPikFlat

app_version = "1.0.3 20210516 10:06"


def main():
    """

    :return:
    """
    # App execution time measurement
    start_time = time.time()

    old_logs = config.basicPath + "/Old_Logs/"
    app_helpers.rotate_log(config.path2log, config.filesToCopy, old_logs)

    # Creating logging infrastructure
    logger = logging.getLogger("pikscraper")
    logger.setLevel(logging.INFO)
    # Create a time rotating logger. Note - for log to rotate, app needs to run all the time!
    handler = TimedRotatingFileHandler(config.path2log, when="d", interval=1, backupCount=30)
    logger.addHandler(handler)

    # We want the first line of the log file to contain only the date and time without handler name, level name etc.
    # Use critical so it will always be shown!
    logger.critical("{0}".format(datetime.now().strftime("%d/%m/%Y %H:%M:%S")))
    logger.info("App Version: {0}".format(app_version))
    logger.info("App config version: {0}".format(config.config_version))
    logger.info("PikScraper package version: {0}".format(PikScraper.PikScraper.get_version()))

    # Create formatter and add it to the handler.
    formatter = logging.Formatter('%(name)s - %(levelname)s - %(funcName)s: %(message)s')
    handler.setFormatter(formatter)

    # Do some print statements - they will be printed to the stdout -> available in cron log if configured.
    print("{0}".format(datetime.now().strftime("%d/%m/%Y %H:%M:%S")))
    print("App Version: {0}".format(app_version))
    print("Config version: {0}".format(config.config_version))
    print("PikScraper package version: {0}".format(PikScraper.PikScraper.get_version()))
    print("Log file available at: {0}".format(config.path2log))
    print("Old Logs folder: {0}".format(old_logs))

    # Load list that contains items found from the first start of the application
    allFlats = app_helpers.load_list_of_flats(config.allFlatsFile)

    # Load lists of items that were found in search results made earlier today
    previouslyFound = app_helpers.load_list_of_flats(config.previousSearchFile)
    newFlats = app_helpers.load_list_of_flats(config.newFlatsTodayFile)
    changedFlats = app_helpers.load_list_of_flats(config.changedFlatsTodayFile)
    removedFlats = app_helpers.load_list_of_flats(config.removedFlatsTodayFile)

    # Do some debug prints
    if allFlats is None:
        logger.warning("List of all flats was None!")
    else:
        logger.info("There are total of {0} flats stored locally.".format(len(allFlats)))

    if previouslyFound is None:
        logger.warning("List of previously found flats was None!")
    else:
        logger.info("Previous search had {0} flats.".format(len(previouslyFound)))

    if newFlats is None:
        logger.warning("There are no new flats found today.")
    else:
        logger.info("There are total of: {0} flats added today.".format(len(newFlats)))

    if changedFlats is None:
        logger.warning("There are no changed flats today.")
    else:
        logger.info("There are total of {0} flats changed today.".format(len(changedFlats)))

    if removedFlats is None:
        logger.warning("There are no removed flats today.")
    else:
        logger.info("There are total of {0} flats removed today.".format(len(removedFlats)))

    # Perfect flat is the flat that satisfies all the properties that we are looking for
    perfectFlat = DesiredPikFlat.DesiredPikFlat(config.properties)

    if datetime.now().hour == config.reportTime:
        # Create a daily report once in 24h and send it per e-mail.
        # If needed, some specific search could be done at this point in time.
        logger.info("It's time for report.")

        # Create report
        if app_helpers.create_report_for_flats(newFlats, changedFlats, removedFlats, config.reportPath):
            logger.info("Report created.")

            # Send report to e-mail
            # Provide valid input arguments to this function in order to send e-mail
            mailSubject = "Report for " + datetime.today().strftime('%Y%m%d')
            app_helpers.send_email("your.email@gmail.com", "yourPassowrd", "recipient@email.com", mailSubject,
                                   config.reportPath)

            # Clear the lists so you can start clean in new 24h cycle
            logger.info("Clearing the lists.")
            if newFlats is not None:
                logger.info("Cleaning the list of new flats.")
                newFlats.clear()
                app_helpers.save_list_of_flats(newFlats, config.newFlatsTodayFile)
            else:
                logger.info("There are no new flats today. Nothing to clear.")

            if changedFlats is not None:
                logger.info("Cleaning the list of changed flats.")
                changedFlats.clear()
                app_helpers.save_list_of_flats(changedFlats, config.changedFlatsTodayFile)
            else:
                logger.info("There are no changed flats today. Nothing to clear.")

            if removedFlats is not None:
                logger.info("Cleaning the list of removed flats")
                removedFlats.clear()
                app_helpers.save_list_of_flats(removedFlats, config.removedFlatsTodayFile)
            else:
                logger.info("There are no removed flats today. Nothing to clear.")

        else:
            logger.info("Failed creating report. Please check the logs.")

    # Start new search
    dateAdded = dateOfChange = dateOfRemoval = datetime.now()
    scraper = PikScraper.PikScraper(config.link, 0)
    logger.info("Currently active flats {0}".format(scraper.get_items_available()))
    currentItems = scraper.get_all_flats_from_search_results()

    if previouslyFound is None:
        logger.warning("Is this the first run of the app?")
        for i in range(len(currentItems)):
            scraper.update_flat_details(currentItems[i])
            result = perfectFlat.satisfies(currentItems[i])
            if result["possible"] == result["satisfied"]:
                logger.info("BINGO! Perfect flat found. Flat ID: {0}".format(currentItems[i].get_item_id()))
            currentItems[i].set_satisfies(result["possible"], result["satisfied"])
            currentItems[i].set_added_locally(dateAdded)
            app_helpers.write_flat_to_file(currentItems[i], config.allFlatsTxt)
        app_helpers.save_list_of_flats(currentItems, config.allFlatsFile)
        app_helpers.save_list_of_flats(currentItems, config.previousSearchFile)
        logger.info("Scraping took {0} seconds".format(time.time() - start_time))
        return

    # Find new flats
    logger.info("Looking for new flats.")
    newFlatsNow = app_helpers.do_list_difference(currentItems, previouslyFound)
    if len(newFlatsNow):
        logger.info("New flats found.")
        # Update details of the new flats
        for i in range(len(newFlatsNow)):
            logger.info("New flat ID: {0}".format(newFlatsNow[i].get_item_id()))
            for j in range(len(currentItems)):
                if newFlatsNow[i] == currentItems[j]:
                    logger.info("Updating flat {0}".format(currentItems[j].get_item_id()))
                    scraper.update_flat_details(currentItems[j])
                    result = perfectFlat.satisfies(currentItems[j])
                    if result["possible"] == result["satisfied"]:
                        logger.info("BINGO! Perfect flat found. Flat ID: {0}".format(currentItems[i].get_item_id()))
                    currentItems[j].set_satisfies(result["possible"], result["satisfied"])
                    currentItems[j].set_added_locally(dateAdded)
                    if newFlats is None:
                        logger.info("This is the first new flat added today.")
                        newFlats = [currentItems[j]]
                    else:
                        newFlats.append(currentItems[j])
                    allFlats.append(currentItems[j])
                    app_helpers.write_flat_to_file(currentItems[j], config.allFlatsTxt)
                    break
    else:
        logger.info("No new flats found in this search.")

    # Find changed flats
    logger.info("Looking for changed flats.")
    for i in range(len(currentItems)):
        for j in range(len(previouslyFound)):
            if currentItems[i] == previouslyFound[j]:
                logger.debug("This flat already exists in previous search results.")
                if currentItems[i].get_price() != previouslyFound[j].get_price():
                    logger.info("Price changed for item {0}".format(currentItems[i].get_item_id()))
                    logger.info("Old price: {0}".format(previouslyFound[j].get_price()))
                    logger.info("New price: {0}".format(currentItems[i].get_price()))

                    # Update the flat details
                    scraper.update_flat_details(currentItems[i])

                    # Update of "old" price
                    currentItems[i].set_old_price(previouslyFound[j].get_price())

                    # Let's see if it satisfies the perfect flat properties with the new price
                    result = perfectFlat.satisfies(currentItems[i])
                    if result["possible"] == result["satisfied"]:
                        logger.info("BINGO! Perfect flat found. Flat ID: {0}".format(currentItems[i].get_item_id()))
                    currentItems[i].set_satisfies(result["possible"], result["satisfied"])

                    # Set the last modified date and time for the item
                    currentItems[i].set_last_modified(dateOfChange)

                    # Let's update date of change and price of the flat in list of all flats
                    for k in range(len(allFlats)):
                        if allFlats[k] == currentItems[i]:
                            logger.info("Flat found in the list of all flats.")
                            allFlats[k].set_last_modified(dateOfChange)
                            allFlats[k].set_price(currentItems[i].get_price())
                            allFlats[k].set_satisfies(result["possible"], result["satisfied"])
                            # Let's add changed flat to a list of changed flats today
                            if changedFlats is None:
                                logger.info("This is the first changed flat of the day.")
                                changedFlats = [allFlats[k]]
                            else:
                                logger.debug("Some flats were already changed today.")
                                changedFlats.append(allFlats[k])
                            break
                break

    # Find removed flats
    logger.info("Looking for removed flats.")
    removedFlatsNow = app_helpers.do_list_difference(previouslyFound, currentItems)
    if len(removedFlatsNow):
        for removedFlat in removedFlatsNow:
            for i in range(len(allFlats)):
                if allFlats[i] == removedFlat:
                    allFlats[i].set_removed(dateOfRemoval)
                    if removedFlats is None:
                        logger.debug("This is the first flat that was removed today.")
                        removedFlats = [allFlats[i]]
                    else:
                        logger.debug("Some flats were already removed today.")
                        removedFlats.append(allFlats[i])
                    break
    else:
        logger.info("No removed flats in this search.")

    app_helpers.save_list_of_flats(allFlats, config.allFlatsFile)
    app_helpers.save_list_of_flats(currentItems, config.previousSearchFile)
    app_helpers.save_list_of_flats(newFlats, config.newFlatsTodayFile)
    app_helpers.save_list_of_flats(changedFlats, config.changedFlatsTodayFile)
    app_helpers.save_list_of_flats(removedFlats, config.removedFlatsTodayFile)

    logger.info("Scraping took {0} seconds".format(time.time() - start_time))
    return


# ----------------------------------------------------------------------
if __name__ == "__main__":
    main()
