#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from datetime import datetime

config_version = "0.0.2 20210424 18:06"

# Time at which daily report will be created and sent to e-mail in [0 - 24] format
reportTime = 21

# Basic parameters used for scraping
city = "Tz"
link = "https://www.olx.ba/pretraga?id=23&kategorija=23&stanje=0&vrstapregleda=tabela&sort_order=desc&sort_po=datum&kanton=3&grad%5B%5D=4944&vrsta=samoprodaja"

# Path to different files
basicPath = os.path.dirname(os.path.realpath(__file__))

# Log file
path2log = basicPath + '/log_{0}.txt'.format(city)

# Paths to files containing previous search results
previousSearchFile = basicPath + '/pik_Tz.artikli'
newFlatsTodayFile = basicPath + '/pik_Tz_novi.artikli'
changedFlatsTodayFile = basicPath + '/pik_Tz_promjene.artikli'
removedFlatsTodayFile = basicPath + '/pik_Tz_uklonjeni.artikli'
allFlatsFile = basicPath + '/pik_Tz_svi.artikli'

# Paths to text files containing all items
allFlatsTxt = basicPath + '/stanovi_Tz.txt'
allChangesTxt = basicPath + '/promjene_Tz.txt'

# Report path
# yesterday = datetime.today() - timedelta(days=1)
today = datetime.today()
reportPath = basicPath + "/" + today.strftime('%Y%m%d') + "-report.txt"

# Create the list of files that will be copied to a folder with old logs
filesToCopy = [newFlatsTodayFile, changedFlatsTodayFile, removedFlatsTodayFile, allFlatsFile]

properties = {
        "location": "Tuzla",
        "minPrice": 75000,
        "maxPrice": 155000,
        "minArea": 50,
        "maxArea": 90,
        "minNumberOfRooms": 2,
        "maxNumberOfRooms": 5,
        "balcony": True,
        "basement": True,
        "parking": True,
        "minFloor": 3,
        "maxFloor": 9,
        "furnished": True,
        "elevator": True,
        "renovated": True
    }
