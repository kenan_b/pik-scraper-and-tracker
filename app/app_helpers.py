#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import pickle
from typing import List
from os import path
from os import makedirs
import shutil
import datetime
from pikscraper import PikFlat

logger = logging.getLogger("pikscraper")


def write_flat_to_file(item: PikFlat.PikFlat, file: str) -> bool:
    """
    Writes PikFlat object to a text file.

    Writes the string representation of PikFlat object to a text file.
    If file does not exists, it will be created. If the file exists, item will be appended to the end of the file.

    :param item: PikFlat object that will be written to ta file.
    :type item: PikFlat

    :param file: path to a file
    :type file: str

    :return: True if everything was ok, False otherwise
    :rtype: bool
    """
    logger.debug("Entered")
    logger.info("Adding item to file {0}".format(file))
    if not path.exists(file):
        logger.warning("File {0} does not exists. It will be created.".format(file))
    try:
        with open(file, 'a+', encoding='utf8') as itemsFile:
            itemsFile.write(item.to_string())
            itemsFile.write("**********************************\n")
    except Exception as error:
        logger.error("Unable to save item to a file. Error: {0}".format(error))
        return False
    logger.debug("Item saved to file {0}".format(file))
    logger.debug("Leaving")
    return True


def load_list_of_flats(path2file: str):
    """
    Loads Python list object which contains PikFlat objects as list elements.

    :param path2file: path to a file that will be loaded into a Python list.
    :type path2file: str

    :return: List of PikItem objects or None if file at given path does not exist.
    """
    logger.debug("Entered")
    logger.debug("Loading items to a list from file {0}".format(path2file))
    if path.exists(path2file):
        with open(path2file, 'rb') as file:
            items = pickle.load(file)
            logger.debug("Items loaded. Number of items: {0}".format(len(items)))
            logger.debug("Leaving")
            return items
    else:
        logger.warning("File does not exist! Path: {0}".format(path2file))
        logger.debug("Leaving")
        return None


def save_list_of_flats(items: List[PikFlat.PikFlat], path2file: str) -> bool:
    """
    Saves Python List object that contains PikFlat objects into a binary file.

    :param items: list object that will be saved to a file.
    :type items: List[PikFlat]
    :param path2file: path to a location where the list object will be saved. If a file exists on this location, it
    will be overwritten.
    :type path2file: str

    :return: True if items were saved to a file.
    """
    logger.debug("Entered")
    logger.debug("Saving list to file: {0}".format(path2file))
    if items is None:
        logger.warning("List of items does not exist (is None). I don't have anything to save.")
        logger.debug("Leaving")
        return False
    logger.debug("Items in the list: {0}".format(len(items)))

    with open(path2file, 'wb') as pikFlatsFile:
        pickle.dump(items, pikFlatsFile)
    logger.debug("List saved to file.")
    logger.debug("Leaving")
    return True


def rotate_log(path2log, listOfFiles, oldLogsFolderName="Old_Logs/"):
    """
    Creates a new folder where it moves log file and copies other files from the list.

    Log file (specified with path2log) must have a date and time on first line inside the file, with specific formating.
    Example: 25/02/2020 09:46:32
    This function reads this line, checks that today's date is not the same as the date from the file, and if it is not
    the same date, than it moves the log file to a newly created folder and copies files specified by their paths in the
    list of files.
    Note: log file will not be copied but moved! Other files (from 'listOfFiles') will be copied.

    :param path2log: path to a log file which contains date and time on the first line in format: dd/mm/yyyy hh:mm:ss
    :type path2log: str
    :param listOfFiles: paths to files that will be copied to a new directory.
    :type listOfFiles list[str]
    :param oldLogsFolderName: Path to the folder where files will be moved or copied!
    :type oldLogsFolderName: str

    :return: None
    """
    logger.debug("Entered")
    if path.exists(path2log):
        logFile = open(path2log, 'r', encoding='utf8')
        line = logFile.readline()
        if len(line):
            logDayStr = line[0:2]
            logDayInt = int(logDayStr)
            logMonthStr = line[3:5]
            logYearStr = line[6:10]
        else:
            logger.error("Unable to rotat logs. It seems that the log file is empty.")
            logFile.close()
            logger.debug("Leaving")
            return
        logFile.close()
    else:
        print("Error! Log file does not exist!")
        logger.debug("Leaving")
        return

    today = datetime.datetime.now().day

    if logDayInt != today:
        folderName = oldLogsFolderName + "/" + logYearStr + logMonthStr + logDayStr
        print("Creating new log folder: {0}".format(folderName))
        makedirs(folderName, exist_ok=True)
        try:
            shutil.move(path2log, folderName)
        except OSError as err:
            print("Failed moving log file to a new location. Error: {0}\n".format(err))
        for file in listOfFiles:
            if path.exists(file):
                print("Copying file '{0}'".format(file))
                shutil.copy(file, folderName)
            else:
                print("Warning! File {0} does not exists and will not be copied.".format(file))
    else:
        print("Dates are the same! Nothing will be copied or moved!")

    logger.debug("Leaving")
    return


def do_list_difference(list1: List, list2: List) -> List:
    """
    Provides list of elements that exist in one list (list1) but don't exist in another list (list2).

    :param list1: list of elements whose elements we want to extract
    :type list1: List of comparable items
    :param list2: list of elements that we are comparing against
    :type list2: List of comparable items
    :return: List that contains elements of list1 that don't exist in list2
    :rtype: List of elements that have the same type as extracted elements of list1
    """
    logging.debug("Entered")

    if (not isinstance(list1, list)) or (not isinstance(list2, list)):
        logging.warning("'list1' or 'list2' is not of 'list' type. Returning empty list!")
        # TODO raise exception here?
        # raise ValueError("Please make sure that both arguments are of list type.")
        return []

    logging.debug("Number of items in 'list1': {0}".format(len(list1)))
    logging.debug("Number of items in 'list2': {0}".format(len(list2)))

    if (len(list1) == 0) or (len(list2) == 0):
        logging.info("'list1' or 'list2' is empty.")
        return list1

    list3 = []
    for elementOfL1 in list1:
        if elementOfL1 not in list2:
            logging.debug("Element {} exists in 'list1' but not in 'list2'.".format(elementOfL1))
            list3.append(elementOfL1)

    logging.debug("There are total of {0} elements that exist in 'list1' but not in 'list2'.".format(len(list3)))
    logging.debug("Leaving")
    return list3


def create_report_for_flats(newFlats: List[PikFlat.PikFlat], changedFlats: List[PikFlat.PikFlat],
                            removedFlats: List[PikFlat.PikFlat], path2file: str) -> bool:
    """
    Creates a text file and writes items from the lists to that file.

    :param newFlats: contains new flats (flats added on this day)
    :type newFlats: : List[PikFlat.PikFlat]
    :param changedFlats: contains flats that have been changed today.
    :type changedFlats: : List[PikFlat.PikFlat]
    :param removedFlats: contains flats that have been removed in the past 24h
    :type removedFlats: : List[PikFlat.PikFlat]
    :param path2file: Path to a file where the report will be written.
    :type path2file: str

    :return: True if everything went well and report was created, false otherwise.
    :rtype: bool
    """
    logger.debug("Entered.")
    logger.debug("Creating report in file: {0}".format(path2file))
    try:
        f = open(path2file, mode="a+", encoding="utf-8")
    except OSError:
        logger.error("Could not open/create file to write report to it.")
        logger.debug("Leaving.")
        return False
    if newFlats is None:
        f.write("No new flats today.\n")
    else:
        f.write("New Flats: {0}\n".format(len(newFlats)))
    if changedFlats is None:
        f.write("No changed flats.\n")
    else:
        f.write("Changed flats: {0}\n".format(len(changedFlats)))
    if removedFlats is None:
        f.write("No removed flats\n")
    else:
        f.write("Removed flats: {0}\n".format(len(removedFlats)))
    f.write("\n -------------------------- \n")
    f.write("New flats:\n")
    if newFlats is not None:
        for flat in newFlats:
            text = "{0}\n".format(flat.get_title())
            satisfies = flat.get_satisfies()
            text += "{0} of {1}\n".format(satisfies["satisfied"], satisfies["possible"])
            text += "Price: {0}\n".format(flat.get_price())
            text += "Price per m^2: {0}\n".format(flat.get_price_per_meter())
            text += "Area: {0}\n".format(flat.get_area())
            text += "Number of rooms: {0}\n".format(flat.get_number_of_rooms())
            text += "{0}\n".format(flat.get_link())
            text += "******************************\n"
            f.write(text)

    f.write("\n -------------------------- \n")
    f.write("Changed flats:\n")
    if changedFlats is not None:
        for flat in changedFlats:
            text = "{0}\n".format(flat.get_title())
            satisfies = flat.get_satisfies()
            text += "{0} of {1}\n".format(satisfies["satisfied"], satisfies["possible"])
            text += "New price: {0}\n".format(flat.get_price())
            text += "Old price: {0}\n".format(flat.get_old_price())
            text += "Number of price changes {0}\n".format(flat.get_number_of_price_changes())
            text += "All prices: {0}\n".format(flat.get_all_old_prices())
            text += "{0}\n".format(flat.get_link())
            text += "******************************\n"
            f.write(text)

    f.write("\n -------------------------- \n")
    f.write("Removed flats:\n")
    if removedFlats is not None:
        for flat in removedFlats:
            text = "{0}\n".format(flat.get_title())
            satisfies = flat.get_satisfies()
            text += "{0} of {1}\n".format(satisfies["satisfied"], satisfies["possible"])
            text += "Price: {0}\n".format(flat.get_price())
            text += "Area: {0}\n".format(flat.get_area())
            text += "Price per m^2: {0}\n".format(flat.get_price_per_meter())
            if flat.get_number_of_price_changes():
                text += "Number of price changes {0}\n".format(flat.get_number_of_price_changes())
                text += "All prices: {0}\n".format(flat.get_all_old_prices())
            text += "******************************\n"
            f.write(text)
    f.close()
    logger.debug("Leaving.")
    return True


def send_email(username: str, password: str, messageReceiver: str, messageSubject: str, fileToSend: str) -> bool:
    """
    Sends e-mail message (via G-Mail) to specified e-mail receiver.

    Content of the e-mail is provided in the text file. Path to this file is specified in the "fileToSend" argument.

    :param username: username to authenticate with the server
    :type username: str
    :param password: password to authenticate with the server
    :type password: str
    :param messageReceiver: e-mail address of the recipient of the e-mail
    :type messageReceiver: str
    :param messageSubject: string that will be used as a subject line
    :type messageSubject: str
    :param fileToSend: path to a file that will be loaded and sent
    :type fileToSend: str

    :return: True if e-mail was sent successfully, False otherwise.
    :rtype: bool
    """
    logger.debug("Entered.")
    import smtplib
    from smtplib import SMTPException
    from email.message import EmailMessage
    smtpServerUser = username
    smtpServerPass = password
    emailSender = username

    if not path.exists(fileToSend):
        logger.error("File with content of e-mail does not exist. E-mail will not be sent.")
        logger.debug("Leaving.")
        return False

    with open(fileToSend, mode='r', encoding="utf-8") as file:
        messageContent = file.read()

    # Create e-mail message
    msg = EmailMessage()
    msg.set_content(messageContent)
    msg['Subject'] = messageSubject
    msg['From'] = emailSender
    msg['To'] = messageReceiver

    try:
        server = smtplib.SMTP('smtp.gmail.com', 587)
    except SMTPException as why:
        logger.error("Error! Failed creating SMTP connection: {0}".format(why))
        logger.debug("Leaving.")
        return False

    server.ehlo()
    server.starttls()
    server.ehlo()

    try:
        server.login(smtpServerUser, smtpServerPass)
    except SMTPException as why:
        logger.error("Error! Failed to authenticate with SMTP server: {0}".format(why))
        logger.debug("Leaving.")
        return False

    try:
        server.send_message(msg)
        logger.info("Email message sent to: {0}\n".format(messageReceiver))
    except SMTPException as why:
        logger.error("Error! Failed sending email: {0}".format(why))
        logger.debug("Leaving.")
        return False

    server.quit()
    logger.debug("Leaving.")
    return True
