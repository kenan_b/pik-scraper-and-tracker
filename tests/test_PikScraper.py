#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pikscraper import PikScraper


def test_01_init():
    """

    :return:
    """
    link = "https://www.olx.ba/pretraga?id=23&kategorija=23&stanje=0&vrstapregleda=tabela&sort_order=desc&sort_po=datum&kanton=3&grad%5B%5D=4944&vrsta=samoprodaja"
    myScraper = PikScraper.PikScraper(link, 1)
    foundFlats = myScraper.get_all_flats_from_search_results()
    detailedFlat = myScraper.update_flat_details(foundFlats[0])
    assert myScraper.pagesToScrape == 12, "It must be 12"
