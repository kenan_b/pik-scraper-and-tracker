#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pytest
from datetime import datetime
from pikscraper import PikFlat, DesiredPikFlat


@pytest.fixture
def flat_1() -> PikFlat.PikFlat:
    """

    :return:
    """
    properties = {
        "itemID": 10,
        "link": "www.example.com",
        "location": "Location1",
        "title": "Item1 Title",
        "subtitle": "Item1 Subtitle",
        "price": 100000,
        "publishedOn": datetime.now(),
        "publishedBy": "Owner1",
        "oldPrice": [105000],
        "addedLocally": datetime.now(),
        "lastModified": None,
        "removed": None,
        "renewed": None,
        "area": 65,
        "numberOfRooms": 3,
        "typeOfHeating": "Public Heating",
        "builtIn": "New",
        "balcony": True,
        "basement": True,
        "parking": True,
        "address": "Address 1",
        "floor": 5,
        "furnished": True,
        "elevator": True,
        "renovated": True
    }
    flat = PikFlat.PikFlat(properties)
    return flat


@pytest.fixture
def desired_flat_1() -> DesiredPikFlat.DesiredPikFlat:
    """

    :return:
    """
    properties = {
        "location": "Location1",
        "minPrice": 75000,
        "maxPrice": 120000,
        "minArea": 55,
        "maxArea": 85,
        "minNumberOfRooms": 2,
        "maxNumberOfRooms": 3,
        "balcony": True,
        "basement": True,
        "parking": True,
        "minFloor": 3,
        "maxFloor": 7,
        "furnished": True,
        "elevator": True,
        "renovated": True
    }
    item = DesiredPikFlat.DesiredPikFlat(properties)
    return item


def test_01_flat_satisfies_averything(flat_1: PikFlat.PikFlat, desired_flat_1: DesiredPikFlat.DesiredPikFlat) -> None:
    """

    :param flat_1:
    :param desired_flat_1:
    :return:
    """
    result = desired_flat_1.satisfies(flat_1)
    flat_1.set_satisfies(result["possible"], result["satisfied"])
    possible = flat_1.get_satisfies()
    assert (possible["possible"] == 11) and (possible["satisfied"] == 11), "11 properties of the desired flat are " \
                                                                           "possible to satisfy and all of them are " \
                                                                           "satisfied"


def test_02_max_price_not_satisfied(flat_1: PikFlat.PikFlat, desired_flat_1: DesiredPikFlat.DesiredPikFlat) -> None:
    """

    :param flat_1:
    :param desired_flat_1:
    :return:
    """
    desired_flat_1.set_max_price(flat_1.get_price()-1)
    result = desired_flat_1.satisfies(flat_1)
    flat_1.set_satisfies(result["possible"], result["satisfied"])
    possible = flat_1.get_satisfies()
    assert (possible["possible"] == 11) and (possible["satisfied"] == 10), "11 properties of the desired flat are " \
                                                                           "possible to satisfy and 10 of them are " \
                                                                           "satisfied"

