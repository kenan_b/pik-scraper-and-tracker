#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pytest
from datetime import  datetime
from pikscraper import PikItem


@pytest.fixture
def item_1() -> PikItem.PikItem:
    """
    Creates a PikItem object.

    Creates a PikItem object with specific properties.

    :return: PikItem object
    """
    properties = {
        "itemID": 1,
        "link": "www.example1.com",
        "location": "Location1",
        "title": "Item1 Title",
        "subtitle": "Item1 Subtitle",
        "price": 100000,
        "publishedOn": datetime.now(),
        "publishedBy": "Owner 1",
        "oldPrice": None,  # Old price should be a list containing at least 1 element or None
        "addedLocally": datetime.now(),
        "lastModified": datetime.now(),
        "removed": None
    }
    item = PikItem.PikItem(properties)
    return item


@pytest.fixture
def item_2():
    """
    Creates a PikItem object.

    Creates a PikItem object with specific properties.

    :return: PikItem object
    """
    properties = {
        "itemID": 2,
        "link": "www.example2.com",
        "location": "Location 2",
        "title": "Item2 Title",
        "subtitle": "Item2 Subtitle",
        "price": 200000,
        "publishedOn": datetime.now(),
        "publishedBy": "Owner 2",
        "oldPrice": [198000],  # Old price should be a list containing at least 1 element or None
        "addedLocally": datetime.now(),
        "lastModified": datetime.now(),
        "removed": None
    }
    item = PikItem.PikItem(properties)
    return item


def test_01_eq(item_1):
    """
    Tests __eq__ method of the PikItem class.

    Test is passed if calling == operator on two equal object returns True

    :return:
    """
    item_2 = item_1
    assert item_1 == item_2, "They must be the same, as they are initialized with the same properties"


def test_02_eq(item_1, item_2):
    """
    Tests __eq__ method of the PikItem class.

    Test is passed if calling == operator on two different objects returns False

    :return:
    """
    result = item_1 == item_2
    assert result == False, "Items have different ID, se they are different."


def test_03_eq(item_1):
    """
    Tests __eq__ method of the PikItem class

    Test is passed if comparing PikItem object to an integer object returns false.

    :param item_1:
    :return:
    """
    result = item_1 == 10
    assert result == False, "Comparing PikItem with other object types must return false!"


def test_04_eq(item_1):
    """
    Tests __eq__ method of the PikItem class

    Test is passed if comparing PikItem object to "None" returns fase.

    :param item_1:
    :return:
    """
    result = item_1 is None
    assert result == False, "Comparing PikItem with None object types must return false!"


def test_05_number_of_price_changes(item_1):
    """
    
    :param item_1: 
    :return: 
    """
    item_1.set_price("15")
    assert item_1.get_number_of_price_changes() == 1, "Price was changed only once!"


def test_06_number_of_price_changes(item_1):
    """

    :param item_1:
    :return:
    """
    assert item_1.get_number_of_price_changes() == 0, "Price was not changed at all"


def test_07_number_of_price_changes(item_1):
    """

    :param item_1:
    :return:
    """
    item_1.set_price("15")
    item_1.set_price("120")
    assert  item_1.get_number_of_price_changes() == 2, "Price was changed 2 times"


def test_08_get_old_price(item_1):
    """

    :param item_1:
    :return:
    """
    assert item_1.get_old_price() == "None", "There is no old price"


def test_09_set_price_get_old_price(item_1):
    """

    :param item_1:
    :return:
    """
    item_1.set_price("15")
    item_1.set_price("120")
    assert item_1.get_old_price() == "15", "Last old price was 15, current price is 120"


def test_10_set_price_get_price(item_1):
    """

    :param item_1:
    :return:
    """
    item_1.set_price("15")
    assert item_1.get_price() == "15", "Current price is 15, old price is the price that was set in the constructor"


def test_11_get_old_price(item_2):
    """

    :param item_2:
    :return:
    """
    assert item_2.get_number_of_price_changes() == 1, "Old price was set in constructor."
