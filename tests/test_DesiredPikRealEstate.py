#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pytest
from datetime import datetime
from pikscraper import PikRealEstate, DesiredPikRealEstate


@pytest.fixture
def real_estate_1() -> PikRealEstate.PikRealEstate:
    """
    Creates a PikItem object.

    Creates a PikItem object with specific properties.

    :return: PikItem object
    """
    properties = {
        "itemID": 1,
        "link": "link1",
        "location": "Location1",
        "title": "title 1",
        "subtitle": "subtitle 1",
        "price": 100000,
        "publishedOn": datetime.now(),
        "publishedBy": "Owner 1",
        "oldPrice": [105000],
        "addedLocally": None,
        "lastModified": None,
        "removed": None,
        "renewed": None,
        "area": 55.4,
        "numberOfRooms": 2,
        "typeOfHeating": "Heating 1",
        "builtIn": "2000 - 2005",
        "balcony": True,
        "basement": True,
        "parking": True,
        "address": "Address 1",
        "floor": 6,
        "furnished": True,
        "elevator": True,
        "renovated": True
    }
    item = PikRealEstate.PikRealEstate(properties)
    return item


@pytest.fixture
def desiredRealEstate_1() -> DesiredPikRealEstate.DesiredPikRealEstate:
    """

    :return:
    """
    properties = {
        "location": "Location1",
        "minPrice": 75000,
        "maxPrice": 120000,
        "minArea": 55,
        "maxArea": 85,
        "minNumberOfRooms": 2,
        "maxNumberOfRooms": 3,
        "balcony": True,
        "basement": True,
        "parking": True
    }
    item = DesiredPikRealEstate.DesiredPikRealEstate(properties)
    return item


def test_01_satisfies_everything(real_estate_1: PikRealEstate.PikRealEstate,
                                 desiredRealEstate_1: DesiredPikRealEstate.DesiredPikRealEstate):
    """
    """
    satisfied = desiredRealEstate_1.satisfies(real_estate_1)
    assert satisfied["satisfied"] == 7, "All of them are satisfied."
