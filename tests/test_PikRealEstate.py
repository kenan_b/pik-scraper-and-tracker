#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pytest
from pikscraper import PikRealEstate
from datetime import datetime


@pytest.fixture
def real_estate_1() -> PikRealEstate.PikRealEstate:
    """
    Creates a PikItem object.

    Creates a PikItem object with specific properties.

    :return: PikItem object
    """
    properties = {
        "itemID": 1,
        "link": "link1",
        "location": "location1",
        "title": "title 1",
        "subtitle": "subtitle 1",
        "price": 100000,
        "publishedOn": datetime.now(),
        "publishedBy": "Owner 1",
        "oldPrice": [105000],
        "addedLocally": None,
        "lastModified": None,
        "removed": None,
        "renewed": None,
        "area": 55.4,
        "numberOfRooms": 2,
        "typeOfHeating": "Heating 1",
        "builtIn": "2000 - 2005",
        "balcony": True,
        "basement": True,
        "parking": True,
        "address": "Address 1",
        "floor": 6,
        "furnished": True,
        "elevator": True,
        "renovated": True
    }
    item = PikRealEstate.PikRealEstate(properties)
    return item


def test_01_price_per_meter(real_estate_1):
    """

    :return:
    """
    price = real_estate_1.get_price() / real_estate_1.get_area()
    assert price == real_estate_1.get_price_per_meter(), "They must match"


def test_02_price_per_meter(real_estate_1):
    """

    :return:
    """
    print("\n Price per meter: {0}".format(real_estate_1.get_price_per_meter()))
    real_estate_1.set_price(0)
    print("\n Price per meter: {0}".format(real_estate_1.get_price_per_meter()))
    assert real_estate_1.get_price_per_meter() == 0, "They must match"


def test_03_price_per_meter(real_estate_1):
    """

    :return:
    """
    print("\n Price per meter: {0}".format(real_estate_1.get_price_per_meter()))
    real_estate_1.set_price(55000)
    print("\n Price per meter: {0}".format(real_estate_1.get_price_per_meter()))
    assert real_estate_1.get_price_per_meter() == real_estate_1.get_price_per_meter(), "They must match"


def test_04_price_per_meter(real_estate_1):
    """

    :return:
    """
    print("\n Price per meter: {0}".format(real_estate_1.get_price_per_meter()))
    real_estate_1.set_area(0)
    print("\n Price per meter: {0}".format(real_estate_1.get_price_per_meter()))
    assert real_estate_1.get_price_per_meter() == 0, "They must match"


def test_05_price_per_meter(real_estate_1):
    """

    :return:
    """
    print("\n Price per meter: {0}".format(real_estate_1.get_price_per_meter()))
    real_estate_1.set_area(100)
    print("\n Price per meter: {0}".format(real_estate_1.get_price_per_meter()))
    assert real_estate_1.get_price_per_meter() == real_estate_1.get_price_per_meter(), "They must match"
