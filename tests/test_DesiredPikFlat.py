#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pytest
from datetime import datetime
from pikscraper import PikFlat, DesiredPikFlat


@pytest.fixture
def flat_1() -> PikFlat.PikFlat:
    """
    :return:
    """
    properties = {
        "itemID": 1,
        "link": "link1",
        "location": "location1",
        "title": "title 1",
        "subtitle": "subtitle 1",
        "price": 100000,
        "publishedOn": datetime.now(),
        "publishedBy": "Owner 1",
        "oldPrice": [105000],
        "addedLocally": datetime.now(),
        "lastModified": None,
        "removed": None,
        "renewed": None,
        "area": 55.4,
        "numberOfRooms": 2,
        "typeOfHeating": "Heating 1",
        "builtIn": "2000 - 2005",
        "balcony": True,
        "basement": True,
        "parking": True,
        "address": "Address 1",
        "floor": 6,
        "furnished": True,
        "elevator": True,
        "renovated": True
    }
    item = PikFlat.PikFlat(properties)
    return item


@pytest.fixture
def desiredFlat_1() -> DesiredPikFlat.DesiredPikFlat:
    """

    :return:
    """
    properties = {
        "location": "location1",
        "minPrice": 75000,
        "maxPrice": 120000,
        "minArea": 55,
        "maxArea": 85,
        "minNumberOfRooms": 2,
        "maxNumberOfRooms": 3,
        "balcony": True,
        "basement": True,
        "parking": True,
        "minFloor": 3,
        "maxFloor": 7,
        "furnished": True,
        "elevator": True,
        "renovated": True
    }
    item = DesiredPikFlat.DesiredPikFlat(properties)
    return item


def test_01_everything_satisfied(flat_1: PikFlat.PikFlat, desiredFlat_1: DesiredPikFlat.DesiredPikFlat):
    """
    
    :param flat_1: 
    :param desiredFlat_1: 
    :return: 
    """
    satisfied = desiredFlat_1.satisfies(flat_1)
    assert satisfied["satisfied"] == 11, "All of them are satisfied."
