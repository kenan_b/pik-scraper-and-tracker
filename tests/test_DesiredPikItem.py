#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pytest
from datetime import datetime
from pikscraper import PikItem, DesiredPikItem


@pytest.fixture
def pikItem_1() -> PikItem.PikItem:
    """
    Creates a PikItem object.

    Creates a PikItem object with specific properties.

    :return: PikItem object
    """
    properties = {
        "itemID": 1,
        "link": "www.example1.com",
        "location": "Location1",
        "title": "Item1 Title",
        "subtitle": "Item1 Subtitle",
        "price": 100000,
        "publishedOn": datetime.now(),
        "publishedBy": "Owner 1",
        "oldPrice": None,  # Old price should be a list containing at least 1 element or None
        "addedLocally": datetime.now(),
        "lastModified": datetime.now(),
        "removed": None
    }
    item = PikItem.PikItem(properties)
    return item


@pytest.fixture
def desiredPikItem_1() -> DesiredPikItem.DesiredPikItem:
    """
    Creates a DesiredPikItem object.

    Creates a DesiredPikItem object with specific properties.

    :return:
    """
    properties = {
        "location": "Location1",
        "minPrice": 75000,
        "maxPrice": 120000
    }
    item = DesiredPikItem.DesiredPikItem(properties)
    return item


def test_01_satisfies_all(pikItem_1: PikItem.PikItem, desiredPikItem_1: DesiredPikItem.DesiredPikItem):
    """
    Tests 'satisfies' method of the DesiredPikItem class.

    Test is passed only if all properties (2 properties) of the DesiredPikItem are satisfied.

    :param pikItem_1: an actual item that we want to check and see how many properties of desired item it satisfies.
    :type pikItem_1: PikItem
    :param desiredPikItem_1: represents an item with all the desired properties.
    :type desiredPikItem_1: DesiredPikItem
    :return: No return value.
    """
    satisfied = desiredPikItem_1.satisfies(pikItem_1)
    assert satisfied["satisfied"] == 2, "Location and price are satisfied, therefore, it must satisfy 2 properties."


def test_02_satisfies_all_with_max_price(pikItem_1: PikItem.PikItem, desiredPikItem_1: DesiredPikItem.DesiredPikItem):
    """
    Tests 'satisfies' method of the DesiredPikItem class when the price of the item is at maximum desired price.

    Test is passed only if all properties (2 properties) of the DesiredPikItem are satisfied.

    :param pikItem_1: an actual item that we want to check and see how many properties of desired item it satisfies.
    :type pikItem_1: PikItem
    :param desiredPikItem_1: represents an item with all the desired properties.
    :type desiredPikItem_1: DesiredPikItem
    :return: No return value.
    """
    pikItem_1.set_price(desiredPikItem_1.get_max_price())
    satisfied = desiredPikItem_1.satisfies(pikItem_1)
    assert satisfied["satisfied"] == 2, "Location and price are satisfied"


def test_03_satisfies_all_with_min_price(pikItem_1: PikItem.PikItem, desiredPikItem_1: DesiredPikItem.DesiredPikItem):
    """
    Tests 'satisfies' method of the DesiredPikItem class when the price of the item is at minimum desired price.

    Test is passed only if all properties (2 properties) of the DesiredPikItem are satisfied.

    :param pikItem_1: an actual item that we want to check and see how many properties of desired item it satisfies.
    :type pikItem_1: PikItem
    :param desiredPikItem_1: represents an item with all the desired properties.
    :type desiredPikItem_1: DesiredPikItem
    :return: No return value.
    """
    pikItem_1.set_price(desiredPikItem_1.get_min_price())
    satisfied = desiredPikItem_1.satisfies(pikItem_1)
    assert satisfied["satisfied"] == 2, "Location and price are satisfied"


def test_04_satisfies_price_too_high(pikItem_1: PikItem.PikItem, desiredPikItem_1: DesiredPikItem.DesiredPikItem):
    """
    Tests 'satisfies' method of the DesiredPikItem class when the price of the item is too high.

    Test is passed only if 1 property of the DesiredPikItem is satisfied.

    :param pikItem_1: an actual item that we want to check and see how many properties of desired item it satisfies.
    :type pikItem_1: PikItem
    :param desiredPikItem_1: represents an item with all the desired properties.
    :type desiredPikItem_1: DesiredPikItem
    :return: No return value.
    """
    pikItem_1.set_price(desiredPikItem_1.get_max_price() + 1)
    satisfied = desiredPikItem_1.satisfies(pikItem_1)
    assert satisfied["satisfied"] == 1, "Location is satisfied"


def test_05_satisfies_price_too_low(pikItem_1: PikItem.PikItem, desiredPikItem_1: DesiredPikItem.DesiredPikItem):
    """
    Tests 'satisfies' method of the DesiredPikItem class when the price of the item is too low.

    Test is passed only if 1 property of the DesiredPikItem is satisfied.

    :param pikItem_1: an actual item that we want to check and see how many properties of desired item it satisfies.
    :type pikItem_1: PikItem
    :param desiredPikItem_1: represents an item with all the desired properties.
    :type desiredPikItem_1: DesiredPikItem
    :return: No return value.
    """
    pikItem_1.set_price(desiredPikItem_1.get_min_price() - 1)
    satisfied = desiredPikItem_1.satisfies(pikItem_1)
    assert satisfied["satisfied"] == 1, "Location is satisfied"


def test_06_satisfies_nothing_satisfied(pikItem_1: PikItem.PikItem, desiredPikItem_1: DesiredPikItem.DesiredPikItem):
    """
    Tests 'satisfies' method of the DesiredPikItem class when the price of the item is too low, and the location is not
    the desired location.

    Test is passed only if 0 properties of the DesiredPikItem are satisfied.

    :param pikItem_1: an actual item that we want to check and see how many properties of desired item it satisfies.
    :type pikItem_1: PikItem
    :param desiredPikItem_1: represents an item with all the desired properties.
    :type desiredPikItem_1: DesiredPikItem
    :return: No return value.
    """
    pikItem_1.set_price(desiredPikItem_1.get_min_price() - 1)
    desiredPikItem_1.set_location("Random")
    satisfied = desiredPikItem_1.satisfies(pikItem_1)
    assert satisfied["satisfied"] == 0, "Nothing is satisfied"


def test_06_satisfies_only_price(pikItem_1: PikItem.PikItem, desiredPikItem_1: DesiredPikItem.DesiredPikItem):
    """
    Tests 'satisfies' method of the DesiredPikItem class when the the location is not the desired location.

    Test is passed only if 1 properties of the DesiredPikItem is satisfied.

    :param pikItem_1: an actual item that we want to check and see how many properties of desired item it satisfies.
    :type pikItem_1: PikItem
    :param desiredPikItem_1: represents an item with all the desired properties.
    :type desiredPikItem_1: DesiredPikItem
    :return: No return value.
    """
    desiredPikItem_1.set_location("Random")
    satisfied = desiredPikItem_1.satisfies(pikItem_1)
    assert satisfied["satisfied"] == 1, "Only price is satisfied."
