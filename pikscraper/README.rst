PikScraper and PikTracker
=========================

This project came to existence when I decided to by a real estate in Bosnia and Herzegovina.

www.pik.ba is a platform where people advertise many different things, starting from consumer electronic to houses and other real estates.

I have started looking for a flat on that website, and at some point keeping track of which flats have been added, removed or modified became time consuming and hard.

Therefore I have decided to build a web scraper in Python which I decided to call PikScraper.
PikScraper is a python package that can be installed to the Python environment and it's scraping services can be used.

Additionally I have created an app, PikTracker, that uses the PikScraper to look for flats and keep track of them.
