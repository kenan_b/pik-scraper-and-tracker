#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from . import DesiredPikRealEstate
from . import PikFlat

logger = logging.getLogger(__name__)


class DesiredPikFlat(DesiredPikRealEstate.DesiredPikRealEstate):
    """

    """

    def __init__(self, properties: dict):
        """

        :param properties:
        :type properties: dict
        """
        logger.debug("Entered")
        self._minFloor = properties["minFloor"]
        self._maxFloor = properties["maxFloor"]
        self._furnished = properties["furnished"]
        self._elevator = properties["elevator"]
        self._renovated = properties["renovated"]
        super().__init__(properties)
        logger.debug("Leaving")

    def get_min_floor(self) -> int:
        """

        :return:
        :rtype: int
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._minFloor

    def set_min_floor(self, floor: int) -> None:
        """

        :param floor:
        :type floor: int
        :return: No return value.
        :rtype: None
        """
        logger.debug("Entered")
        self._minFloor = floor
        logger.debug("Leaving")

    def get_max_floor(self) -> int:
        """

        :return:
        :rtype: int
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._maxFloor

    def set_max_floor(self, floor: int) -> None:
        """

        :param floor:
        :type floor: int
        :return: No return value.
        :rtype: None
        """
        logger.debug("Entered")
        self._maxFloor = floor
        logger.debug("Leaving")

    def get_furnished(self) -> bool:
        """

        :return:
        :rtype: bool
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._furnished

    def set_furnished(self, furnished: bool) -> None:
        """

        :param furnished:
        :type furnished: bool
        :return: No return value.
        :rtype: None
        """
        logger.debug("Entered")
        self._furnished = furnished
        logger.debug("Leaving")

    def get_elevator(self) -> bool:
        """

        :return:
        :rtype: bool
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._elevator

    def set_elevator(self, elevator: bool) -> None:
        """

        :param elevator:
        :type elevator: bool
        :return: No return value.
        :rtype: None
        """
        logger.debug("Entered")
        self._elevator = elevator
        logger.debug("Leaving")

    def get_renovated(self) -> bool:
        """

        :return:
        :rtype: bool
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._renovated

    def set_renovated(self, renovated: bool) -> None:
        """

        :param renovated:
        :type renovated: bool
        :return: No return value.
        :rtype: None
        """
        logger.debug("Entered")
        self._renovated = renovated
        logger.debug("Leaving")

    def satisfies(self, item: PikFlat.PikFlat):
        """

        :param item:
        :type item: PikFlat
        :return:
        """
        logger.debug("Entered")
        logger.debug("Checking if the item {0} satisfies the floor, furniture, elevator and renovated "
                    "properties.".format(item.get_item_id()))
        result = 0
        # Floor
        if (item.get_floor() <= self._maxFloor) and (item.get_floor() >= self._minFloor):
            logger.debug("This flat is on the desired floor.")
            result += 1
        else:
            logger.debug("This flat is NOT on the desired floor.")

        # Furnished
        if self._furnished:
            if item.get_furnished():
                logger.debug("Flat is being sold with furniture, just like we are looking for.")
                result += 1
            else:
                logger.debug("Flat is being sold without furniture, but we are looking for a flat with furniture.")
        else:
            logger.debug("We don't want a furniture.")
            if item.get_furnished():
                logger.debug("but this item has a furniture.")
            else:
                logger.debug("and this item does NOT have a furniture, just like we are looking for.")
                result += 1

        # Elevator
        if self._elevator:
            if item.get_elevator():
                logger.debug("This flat is in a building with elevator, just like we are looking for.")
                result += 1
            else:
                logger.debug("This flat is in a building WITHOUT an elevator. Not what we are looking for.")
        else:
            logger.debug("We don't need an elevator.")
            if item.get_elevator():
                logger.debug("but this flat is in a build with an elevator.")
            else:
                logger.debug("and this flat is in a building that dos NOT have an elevator.")
                result += 1

        # Renovated
        if self._renovated:
            if item.get_renovated():
                logger.debug("This flat is renovated, just like we are looking for.")
                result += 1
            else:
                logger.debug("This flat is NOT renovated. Not what we are looking for.")
        else:
            logger.debug("We don't need a renovated flat.")
            if item.get_renovated():
                logger.debug("but this flat is renovated.")
            else:
                logger.debug("and this flat is not renovated. This matches our expectations.")
                result += 1

        # Create the final result
        superSatisfies = super().satisfies(item)
        possible = superSatisfies["possible"] + 4
        totalResult = superSatisfies["satisfied"] + result
        satisfies = {"possible": possible, "satisfied": totalResult}
        logger.debug("Leaving")
        return satisfies
