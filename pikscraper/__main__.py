#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def main():
    print("Not intended to run as standalone.")
    return


# ----------------------------------------------------------------------
if __name__ == "__main__":
    print("Running package as standalone app...")
    main()