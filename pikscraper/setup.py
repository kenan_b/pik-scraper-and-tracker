import setuptools


with open("README.rst", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pikscraper",
    version="1.1.1",
    author="Kenan Biberkic",
    author_email="<kenan.biberkic@gmail.com>",
    description="Scrapper designed to scrape www.pik.ba web page, with special focus on flats advertised "
                "on the website.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/kenan_b/pik-scraper-and-tracker/src/master/",
    packages=setuptools.find_packages(),
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.5'
)
