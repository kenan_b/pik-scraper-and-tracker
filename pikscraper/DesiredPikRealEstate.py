#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from . import DesiredPikItem
from . import PikRealEstate

logger = logging.getLogger(__name__)


class DesiredPikRealEstate(DesiredPikItem.DesiredPikItem):
    """

    """

    def __init__(self, properties: dict):
        """

        :param properties:
        :type properties: dict
        """
        logger.debug("Entered")
        self._minArea = properties["minArea"]
        self._maxArea = properties["maxArea"]
        self._minNumberOfRooms = properties["minNumberOfRooms"]
        self._maxNumberOfRooms = properties["maxNumberOfRooms"]
        self._balcony = properties["balcony"]
        self._basement = properties["basement"]
        self._parking = properties["parking"]
        super().__init__(properties)
        logger.debug("Leaving")

    def get_min_area(self) -> float:
        """
        Provides the minimum area of the desired real estate.

        :return: minimum area of the desired real estate.
        :rtype: float
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._minArea

    def set_min_area(self, area: float) -> None:
        """
        Sets the minimum area of the desired real estate.

        :param area: minimum area of the desired real estate.
        :type area: float
        :return: No return value.
        :rtype: None
        """
        logger.debug("Entered")
        self._minArea = area
        logger.debug("Leaving")

    def get_max_area(self) -> float:
        """
        Provides the maximum area of the desired real estate.

        :return: maximum area of the desired real estate.
        :rtype: float
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._maxArea

    def set_max_area(self, area: float) -> None:
        """
        Sets the maximum area of the desired real estate.

        :param area: maximum area of the desired real estate.
        :type area: float
        :return: No return value.
        :rtype: None
        """
        logger.debug("Entered")
        self._maxArea = area
        logger.debug("Leaving")

    def get_min_number_of_rooms(self) -> float:
        """
        Provides the minimum number of rooms of the desired real estate.

        :return: minimum number of rooms of the desired real estate.
        :rtype: float
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._minNumberOfRooms

    def set_min_number_of_rooms(self, rooms: float) -> None:
        """
        Sets the minimum number of rooms of the desired real estate.

        :param rooms: minimum number of rooms of the desired real estate.
        :return: No return value
        :rtype: None
        """
        logger.debug("Entered")
        self._minNumberOfRooms = rooms
        logger.debug("Leaving")

    def get_max_number_of_rooms(self) -> float:
        """
        Provides the maximum number of rooms of the desired real estate.

        :return: maximum number of rooms of the desired real estate.
        :rtype: float
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._maxNumberOfRooms

    def set_max_number_of_rooms(self, rooms: float) -> None:
        """
        Sets the maximum number of rooms of the desired real estate.

        :param rooms: maximum number of rooms of the desired real estate.
        :return: No return value
        :rtype: None
        """
        logger.debug("Entered")
        self._maxNumberOfRooms = rooms
        logger.debug("Leaving")

    def get_balcony(self) -> bool:
        """
        Provides information if the desired real estate has balcony.

        :return:
        :rtype: bool
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._balcony

    def set_balcony(self, balcony: bool) -> None:
        """
        Sets the property 'balcony' of the desired real estate.

        :param balcony: desired value
        :type balcony: bool
        :return: No return value.
        :rtype:  None
        """
        logger.debug("Entered")
        self._balcony = balcony
        logger.debug("Leaving")

    def get_basement(self) -> bool:
        """

        :return:
        :rtype:  bool
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._basement

    def set_basement(self, basement: bool) -> None:
        """

        :param basement:
        :type basement: bool
        :return: No return value.
        :rtype:  None
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        self._basement = basement

    def get_parking(self) -> bool:
        """

        :return:
        :rtype: bool
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._parking

    def set_parking(self, parking: bool) -> None:
        """

        :param parking:
        :type parking: bool
        :return: No return value.
        :rtype:  None
        """
        logger.debug("Entered")
        self._parking = parking
        logger.debug("Leaving")

    def satisfies(self, item: PikRealEstate.PikRealEstate):
        """

        :param item:
        :return:
        """
        logger.debug("Entered")
        logger.debug("Checking if the item {0} satisfies the area, number of rooms, balcony, basement and parking "
                    "properties.".format(item.get_item_id()))
        result = 0

        # Check area of the real estate with the desired area
        if (item.get_area() <= self._maxArea) and (item.get_area() >= self._minArea):
            logger.debug("Area of the item is within desired range.")
            result += 1
        else:
            logger.debug("Area of the item is NOT within desired range.")

        # Check number of rooms of the real estate with the desired number of rooms
        if (item.get_number_of_rooms() <= self._maxNumberOfRooms) and \
                (item.get_number_of_rooms() >= self._minNumberOfRooms):
            logger.debug("Number of rooms of the item is within desired range.")
            result += 1
        else:
            logger.debug("Number of rooms of the item is NOT within desired range.")

        # Check balcony
        if self._balcony:
            if item.get_balcony():
                logger.debug("Item has balcony, just like we are looking for.")
                result += 1
            else:
                logger.debug("Item does NOT have a balcony, but we are looking for a balcony.")
        else:
            logger.debug("We don't want a balcony.")
            if item.get_balcony():
                logger.debug("but this item has a balcony.")
            else:
                logger.debug("and this item does NOT have a balcony, just like we are looking for.")
                result += 1

        # Check basement
        if self._basement:
            if item.get_basement():
                logger.debug("Item has basement, just like we are looking for.")
                result += 1
            else:
                logger.debug("Item does NOT have a basement, but we are looking for a basement.")
        else:
            logger.debug("We don't want a basement.")
            if item.get_basement():
                logger.debug("but this item has a basement.")
            else:
                logger.debug("and this item does NOT have a basement, just like we are looking for.")
                result += 1

        # Check parking
        if self._parking:
            if item.get_parking():
                logger.debug("Item has parking, just like we are looking for.")
                result += 1
            else:
                logger.debug("Item does NOT have a parking, but we are looking for a parking.")
        else:
            logger.debug("We don't want a parking.")
            if item.get_parking():
                logger.debug("but this item has a parking.")
            else:
                logger.debug("and this item does NOT have a parking, just like we are looking for.")
                result += 1
        # Create the final result
        superSatisfies = super().satisfies(item)
        possible = superSatisfies["possible"] + 5
        totalResult = superSatisfies["satisfied"] + result
        satisfies = {"possible": possible, "satisfied": totalResult}
        logger.debug("Leaving")
        return satisfies
