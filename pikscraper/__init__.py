#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Presence of __init__.py makes directory a Python package

__author__ = "Kenan Biberkic <kenan.biberkic@gmail.com>"
__status__ = "in development"
__version__ = "1.1.1"
__date__ = "07.08.2022"

__all__ = ['PikItem','PikRealEstate', 'PikFlat', 'DesiredPikItem', 'DesiredPikRealEstate', 'DesiredPikFlat',
           'PikScraper']
