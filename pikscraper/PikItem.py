#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from datetime import date
# TODO Implement following method:  __hash__ method.
# For more details about this method, see this link: https://docs.python.org/2.7/reference/datamodel.html#object.__lt__

logger = logging.getLogger(__name__)


class PikItem:
    """

    """

    def __init__(self, properties: dict):
        """

        :param properties:
        :type properties: dict
        """
        # ToDo - Learn about Python Decorators @property and @Name.setter and built-in method "property"
        logger.debug("Entered")
        # Basic info - obtained from the search results page
        self._itemID = properties["itemID"]
        self._link = properties["link"]
        self._location = properties["location"]
        self._title = properties["title"]
        self._subtitle = properties["subtitle"]
        self._price = properties["price"]
        self._publishedOn = properties["publishedOn"]
        self._publishedBy = properties["publishedBy"]
        # Local values - values not available on the Pik.ba website
        self._oldPrice = properties["oldPrice"]
        self._addedLocally = properties["addedLocally"]
        self._lastModified = properties["lastModified"]
        self._removed = properties["removed"]
        logger.debug("Leaving")

    def __del__(self):
        """
        Class destructor.

        Performs the clean-up once the class object has been deleted.

        :return: None
        """
        pass

    def __eq__(self, other) -> bool:
        """
        This method implements comparison operator.

        This method will be executed when two objects of class PikItem are being compared (using == operator).
        Objects are equal if they have the same id!

        :param other: object to compare against.
        :type other: PikItem
        :return: True if this object and the other object have the same id, False otherwise.
        :rtype: bool
        """
        logger.debug("Entered")
        if other is None:
            logger.debug("Leaving")
            return False

        if not isinstance(other, self.__class__):
            logger.debug("Leaving")
            return False

        logger.debug("Leaving")
        return self._itemID == other.get_item_id()

    def __str__(self) -> str:
        """
        Creates string representation of class object.

        This method gets executed whenever PikItem object is sent to the print() method.

        :return: string representation of object.
        :rtype: string
        """
        logger.debug("Entered")
        text = "ID: {0}\n".format(self._itemID)
        text += "Title: {0}\n".format(self._title)
        text += "Subtitle: {0}\n".format(self._subtitle)
        text += "Link: {0}\n".format(self._link)
        text += "Location: {0}\n".format(self._location)
        text += "Price: {0}\n".format(self._price)
        text += "Published on: {0}\n".format(self._publishedOn)
        text += "Published by: {0}\n".format(self._publishedBy)
        text += "Old price {0}\n".format(self._oldPrice)
        text += "Added locally {0}\n".format(self._addedLocally)
        text += "Last modified {0}\n".format(self._lastModified)
        text += "Removed {0}\n".format(self._removed)
        logger.debug("Leaving")
        return text

    def get_item_id(self) -> int:
        """

        :return: Unique item ID as specified on the website
        :rtype: int
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._itemID

    def get_link(self) -> str:
        """

        :return:
        :rtype: str
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._link

    def get_location(self) -> str:
        """

        :return:
        :rtype: str
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._location

    def get_title(self) -> str:
        """

        :return:
        :rtype: str
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._title

    def get_subtitle(self) -> str:
        """

        :return:
        :rtype: str
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._subtitle

    def get_price(self) -> float:
        """

        :return:
        :rtype: str
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._price

    def set_price(self, price: float) -> None:
        """

        :param price:
        :return:
        """
        logger.debug("Entered")
        logger.debug("Setting old price of the item")
        if isinstance(self._oldPrice, list):
            self._oldPrice.append(self._price)
        else:
            self._oldPrice = [self._price]
        # TODO Set the last modified date at this point because we have just changed 1 property of the item.
        self._price = price
        logger.debug("Leaving")

    def get_number_of_price_changes(self) -> int:
        """

        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        if isinstance(self._oldPrice, list):
            return len(self._oldPrice)
        elif self._oldPrice is None:
            return 0

    def get_published_on(self) -> date:
        """

        :return:
        :rtype: str
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._publishedOn

    def get_published_by(self) -> str:
        """

        :return:
        :rtype: str
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._publishedBy

    def set_published_by(self, owner: str) -> None:
        """

        :param owner:
        :return: No return value
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        self._publishedBy = owner

    def get_old_price(self) -> float:
        """

        :return:
        :rtype: str
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        if isinstance(self._oldPrice, list):
            # We want to return the last known price if someone asks for old price.
            return self._oldPrice[-1]
        else:
            return "None"

    def get_all_old_prices(self):
        """

        :return:
        """
        logger.debug("Entered")
        if isinstance(self._oldPrice, list):
            return self._oldPrice
        else:
            return []

    def set_old_price(self, price: float) -> None:
        """

        :return:
        :rtype: None
        """
        # TODO Check this item. It seems to be redundant.
        logger.debug("Entered")
        logger.debug("Leaving")
        self._oldPrice = price

    def get_added_locally(self) -> date:
        """

        :return:
        :rtype: date
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._addedLocally

    def set_added_locally(self, added: date) -> None:
        """

        :param added:
        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        self._addedLocally = added

    def get_last_modified(self) -> date:
        """

        :return:
        :rtype: date
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._lastModified

    def set_last_modified(self, modified: date) -> None:
        """

        :param modified:
        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        self._lastModified = modified

    def get_removed(self) -> date:
        """

        :return:
        :rtype: date
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._removed

    def set_removed(self, removed: date) -> None:
        """

        :param removed:
        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        self._removed = removed
