#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from . import PikRealEstate

logger = logging.getLogger(__name__)


class PikFlat(PikRealEstate.PikRealEstate):
    """

    """

    def __init__(self, properties: dict):
        """

        :param properties:
        :type properties: dict
        """
        logger.debug("Entered")
        self._address = properties["address"]
        self._floor = properties["floor"]
        self._furnished = properties["furnished"]
        self._elevator = properties["elevator"]
        self._renovated = properties["renovated"]
        super().__init__(properties)
        self._satisfies = {"possible": 0, "satisfied": 0}
        logger.debug("Leaving")

    def __str__(self) -> str:
        """
        Creates string representation of class object.

        This method gets executed whenever PikFlat object is sent to the print() method.

        :return: string representation of object.
        :rtype: string
        """
        logger.debug("Entered")
        text = super().__str__()
        text += "Address: {0}\n".format(self._address)
        text += "Floor: {0}\n".format(self._floor)
        if self._furnished:
            text += "Furnished: Yes\n"
        else:
            text += "Furnished: No\n"
        if self._elevator:
            text += "Elevator: Yes\n"
        else:
            text += "Elevator: No\n"
        if self._renovated:
            text += "Renovated: Yes\n"
        else:
            text += "Renovated: No\n"
        text += "Satisfies {0} out of possible {1}\n".format(self._satisfies["satisfied"], self._satisfies["possible"])

        logger.debug("Leaving")
        return text

    def get_address(self) -> str:
        """

        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._address

    def set_address(self, address: str) -> None:
        """

        :param address:
        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        self._address = address

    def get_floor(self) -> int:
        """

        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._floor

    def set_floor(self, floor: int) -> None:
        """

        :param floor:
        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        self._floor = floor

    def get_furnished(self) -> str:
        """

        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._furnished

    def set_furnished(self, furniture: str) -> None:
        """

        :param furniture:
        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        self._furnished = furniture

    def get_elevator(self) -> bool:
        """

        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._elevator

    def set_elevator(self, elevator: bool) -> None:
        """

        :param elevator:
        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        self._elevator = elevator

    def get_renovated(self) -> bool:
        """

        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._renovated

    def set_renovated(self, renovated: bool) -> None:
        """

        :param renovated:
        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        self._renovated = renovated

    def to_string(self) -> str:
        """

        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self.__str__()

    def set_satisfies(self, possible: int, satisfied: int) -> None:
        """
        Sets the value of how many properties does this flat satisfy of all possible properties.

        :param possible: number of possible properties that can be satisfied.
        :type possible: int
        :param satisfied: number of properties that this flat satisfies
        :type satisfied: int
        :return: No return value.
        :rtype: None
        """
        logger.debug("Entered")
        self._satisfies["possible"] = possible
        self._satisfies["satisfied"] = satisfied
        logger.debug("Leaving")

    def get_satisfies(self) -> dict:
        """
        Provides the information on how many properties of the desired flat this flat satisfies

        :return: Number of possible and satisfied properties of the desired item.
        :rtype: dict['possible', 'satisfied']
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._satisfies
