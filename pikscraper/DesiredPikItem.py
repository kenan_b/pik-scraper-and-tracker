#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from . import PikItem

logger = logging.getLogger(__name__)


class DesiredPikItem:
    """

    """

    def __init__(self, properties: dict):
        """
        Constructor of the DesiredPikItem class.

        Creates a new DesiredPikItem object with the properties defined in the 'properties' argument that is provided
        when class constructor is invoked.

        'properties' argument is a dictionary that must have following keys defined:
        - location
        - minPrice
        - maxPrice
        Location is an string value, while minPrice and maxPrice are float values.

        :param properties:
        :type properties: dict
        """
        logger.debug("Entered")
        self._location = properties["location"]
        self._minPrice = properties["minPrice"]
        self._maxPrice = properties["maxPrice"]
        logger.debug("Leaving")

    def get_location(self) -> str:
        """
        Provide the information on the location of the desired item.

        :return: Location of the desired item.
        :rtype: str
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._location

    def set_location(self, location: str) -> None:
        """
        Sets the location of the desired item.

        :param location: location of the desired item.
        :type location: str
        :return: No return value.
        :rtype: None
        """
        logger.debug("Entered")
        self._location = location
        logger.debug("Leaving")

    def get_min_price(self) -> float:
        """
        Provides the minimum price of the desired item.

        :return: Minimum price of the desired item.
        :rtype: float
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._minPrice

    def set_min_price(self, price: float) -> None:
        """
        Sets the minimum price for the desired item.

        :param price: Minimum price of the desired item.
        :type price: float
        :return: No return value.
        :rtype: None
        """
        logger.debug("Entered")
        self._minPrice = price
        logger.debug("Leaving")

    def get_max_price(self) -> float:
        """
        Provides the maximum price of the desired item.

        :return: Maximum price of the desired item.
        :rtype: float
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._maxPrice

    def set_max_price(self, price: float) -> None:
        """
        Sets the maximum price for the desired item.

        :param price: Maximum price of the desired item.
        :type price: float
        :return: No return value.
        :rtype: None
        """
        logger.debug("Entered")
        self._maxPrice = price
        logger.debug("Leaving")

    def satisfies(self, item: PikItem.PikItem) -> dict:
        """
        Checks which properties of the desired item, does the item satisfy.

        Compares the PikItem with the DesiredPikItem and checks which properties of the DesiredPikItem are satisfied
        by the PikItem.
        DesiredPikItem has total of 3 properties that could be satisfied and those are:
        - location
        - minimal price
        - maximal price

        This method returns a dictionary with the following keys:
        - 'possible'
        - 'satisfied'
        Both keys hold integer values.

        :param item: item that is being checked.
        :type item: PikItem
        :return: Number of possible properties and number of properties that this PikItem satisfies
        :rtype: dict
        """
        logger.debug("Entered")
        logger.debug("Checking if the item {0} satisfies the location and price.".format(item.get_item_id()))
        satisfies = {"possible": 2}
        result = 0
        # Check the location
        if item.get_location().upper() == self._location.upper():
            logger.debug("Satisfies the location.")
            result += 1
        else:
            logger.debug("Does not satisfy location.")
        # Check the price
        if (item.get_price() <= self._maxPrice) and (item.get_price() >= self._minPrice):
            logger.debug("Price is within range.")
            result += 1
        else:
            logger.debug("Does not satisfy price.")
        satisfies["satisfied"] = result
        logger.debug("Item satisfies {0} requirements.".format(result))
        logger.debug("Leaving")
        return satisfies
