#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests
import html
import logging
from requests.exceptions import HTTPError
from datetime import datetime
from typing import List
from . import PikFlat
from . import __version__ as packageVersion

logger = logging.getLogger(__name__)


class PikScraper:
    """

    """
    def __init__(self, link: str, pages: int = 1):
        """

        :param link: starting link
        "type link: string
        :param pages: Number of pages to scrape
        :type pages: int
        """
        logger.debug("Entered")
        logger.debug("Creating PikScraper object. Link {0}".format(link))
        logger.debug("Pages to scrape {0}".format(pages))
        self._page = None
        self._items = []
        self.startingLink = link
        self._page = firstPage = self._get_page(self.startingLink)
        self._itemsAvailable = self._get_number_of_items(firstPage)
        if pages <= 0:
            logger.warning("It is not possible to scrape 0 or less pages.")
            if self._page is not None:
                self.pagesToScrape = self._itemsAvailable // 30
                if (self._itemsAvailable % 30) > 0:
                    self.pagesToScrape += 1
            else:
                logger.warning("Parsing default maximum number of pages.")
                self.pagesToScrape = 10
        else:
            self.pagesToScrape = pages
        del firstPage
        logger.debug("Leaving")

    def __del__(self):
        """
        Class destructor.

        Performs the clean-up once the class object has been deleted.

        :return: None
        """
        pass

    @staticmethod
    def _get_page(link: str) -> str:
        """
        Retrieves the webpage from the Internet.

        Makes a call to the webserver and gets the webpage from the Internet. Uses the address provided in the 'link'
        argument.

        :param link: link to the page whose content is requested (www.example.com)
        :type link: str
        :return:
        """
        logger.debug("Entered")
        logger.debug("Fetching page {0}".format(link))
        headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:103.0) Gecko/20100101 Firefox/103.0'}
        try:
            response = requests.get(link, headers=headers)
        except HTTPError as http_err:
            logger.error("HTTP Error occurred! Error: {0}".format(http_err))
            return None
        except Exception as err:
            logger.error("Error occurred while fetching the website. Error: {0}".format(err))
            return None
        if not response:
            # If you use a Response instance in a conditional expression, it will evaluate to True if the status code
            # was between 200 and 400, and False otherwise. Technical Detail: This Truth Value Test is made possible
            # because __bool__() is an overloaded method on Response.
            logger.error("Making request failed. Status code: {0}".format(response.status_code))
            return None
        logger.debug("Page retrieved")
        response.encoding = 'utf-8'
        logger.debug("Leaving")
        return response.text

    @staticmethod
    def _get_number_of_items(page: str) -> int:
        """
        Scrapes the page of the results for total number of items available.

        Searches the text of the page stored in the self._page for number of results available.
        It is important to know that number of results is only shown on the first page of the results.
        It also shortens the string self._page because all the results are available only in the text after the number
        of results.

        :param page:
        :type page: str
        :return: Returns the number of items found on the page or default value (300).
        :rtype: int
        """
        logger.debug("Entered")
        starting = page.find("brojrezultata")
        if starting == -1:
            logger.warning("Unable to find the number of results on this page.")
            logger.warning("Returning default value for number of items (300).")
            logger.debug("Leaving.")
            return 300

        # Shorten the page string by removing everything before the number of items of the search.
        page = page[starting+15:]
        starting = page.find(">")
        end = page.find("</")
        numberOfItems = page[starting+1:end]

        try:
            numberOfItems = int(numberOfItems)
        except ValueError:
            logger.error("Unable to determine number of items found on the page.")
            logger.warning("Returning default value for number of items (300).")
            logger.debug("Leaving.")
            return 300

        logger.debug("Number of items found: {0}".format(numberOfItems))
        logger.debug("Leaving.")
        return numberOfItems

    def _get_link_to_next_page(self) -> str:
        """
        Parses the provided page for a link to next page.

        Parses the content of the webpage searching for the link to next page. If this is the last page, it returns
        None. Link to next page is constructed from the base string and content found on the provided page.
        Note: it shortens the string of the page once the link is found.

        :return: constructed link to next page or None if this is the last page.
        :rtype: str
        """
        logger.debug("Entered.")
        start = self._page.find("rel='next'")
        if start == -1:
            logger.warning("There is no link to next page. Last page reached?")
            logger.debug("Leaving")
            return None

        # Shorten the page string
        self._page = self._page[start+10:]
        start = self._page.find('href=')
        end = self._page.find("style")
        link = "https://www.olx.ba" + self._page[start+6:end-2]
        logger.debug("Link to next page: {0}".format(link))
        logger.debug("Leaving")
        return link

    def get_items_available(self) -> int:
        """
        Provides number of items available on the website as reported by the website.

        :return:
        :rtype: int
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._itemsAvailable

    def get_all_flats_from_search_results(self) -> List[PikFlat.PikFlat]:
        """
        Provides a list of items found in the search results.

        Parses the page with search results, extracting items that are advertised on the website.
        It also goes through the search results, fetching required number of pages (specified in constructor).
        Furthermore, it retrieves the page of every item found on the page with search results, and fetches detailed
        information about every item.
        Items are stored in a list of items.

        :return: list of items found on the website
        :rtype: list[PikFlat.PikFlat]
        """
        logger.debug("Entered")

        processedPages = 0
        while processedPages < self.pagesToScrape:
            logger.debug("Processing page {0}".format(processedPages+1))
            processedPages += 1

            # Check that results are available at this page
            start = self._page.find('id="rezultatipretrage"')
            if start == -1:
                logger.error("Error! Parsing page failed. Unable to find the results of query.")
                logger.debug("Leaving")
                return self._items

            content = self._page[(start + 23):]  # id="rezultatipretrage"> -> 23 characters

            # Extract items from page, one by one
            # Every time the loop is entered, we are fetching information on 1 item until we get to the end of search
            # results. Then we try to get the link to the next page with results and fetch that page.
            while len(content) > 50:
                # Check that we have not reached the end - after the last item, there is plenty of more content on
                # the page
                start = content.find('class="na"')
                if start == -1:
                    logger.debug("Finished parsing this page.")
                    link = self._get_link_to_next_page()
                    if link is not None:
                        self._page = self._get_page(link)
                    break

                # When we finish an item, some more characters are left that belong to him. We will remove those chars.
                start = content.find('class="listitem', 0)
                content = content[start:]

                # Get the basic info about the item from the page with search results
                # Item ID
                start = content.find('id="art', 0)
                end = content.find('>', start)
                itemIdStr = content[start + 8:end - 2]  # start + 8 because we don't need art_ in the article number
                try:
                    itemId = int(itemIdStr)
                except ValueError:
                    logger.error("Unable to get item ID as integer.")
                    logger.warning("Using default item ID: 0.")
                    itemId = 0
                content = content[(end + 1):]

                # Link to the item
                start = content.find('https', 0)
                end = content.find('>', start)
                link = content[start:end - 2]
                content = content[(end + 1):]

                # Location
                start = content.find('lokacijadiv', 0)
                start = content.find('/span>', start)
                end = content.find('</div>', start)
                location = html.unescape(html.unescape(content[start + 6: end]))
                content = content[(end + 6):]

                # Title
                start = content.find('class="na"', 0)
                end = content.find('</p>', start)
                title = html.unescape(html.unescape(content[start + 11:end]))
                content = content[(end + 4):]

                # Sub-title
                start = content.find('class="pna"', 0)
                end = content.find('</div>', start + 12)
                if (start + 13) == end:
                    logger.debug("No subtitle")
                    subtitle = None
                else:
                    subtitle = html.unescape(html.unescape(content[start + 12:end]))
                    content = content[(end + 6):]

                # Price
                start = content.find('datum', 0)
                content = content[(start + 5):]
                start = content.find('span', 0)
                end = content.find('</span', start)
                # If the price is "Po dogovoru" then it requires special treatment!
                price = content[start + 6:end]
                logger.debug("Processing price string: {0}".format(price))
                temp = price.find('color')
                if temp != -1:
                    # Price is most likely "PO DOGOVORU"
                    start = price.find('">')
                    price = price[start + 2:]
                    price = price.lstrip()
                    price = price.rstrip()
                    price = price.upper()
                    if price == "PO DOGOVORU":
                        logger.debug("Price of the item is negotiable.")
                        price = 0
                    else:
                        logger.warning("Unable to determine the price. Setting it to zero.")
                        logger.warning("Price was set to: {0}".format(price))
                        price = 0
                else:
                    price = content[start + 6:end]
                    price = price.replace(".", "")
                    price = price.rstrip(" KM")
                    try:
                        price = int(price)
                    except ValueError:
                        logger.error("Unable to determine the price as an integer.")
                        logger.warning("Setting the price to zero.")
                        price = 0

                content = content[(end + 7):]

                # Date and time
                start = content.find('title=', 0)
                end = content.find('data-', start)
                date_str = content[start + 7:end - 2]
                date = datetime.strptime(date_str, "%d.%m.%Y. u %H:%M")

                properties = {
                    "itemID": itemId,
                    "link": link,
                    "location": location,
                    "title": title,
                    "subtitle": subtitle,
                    "price": price,
                    "publishedOn": date,
                    "publishedBy": None,
                    "oldPrice": None,
                    "addedLocally": None,
                    "lastModified": None,
                    "removed": None,
                    "renewed": None,
                    "area": None,
                    "numberOfRooms": None,
                    "typeOfHeating": None,
                    "builtIn": None,
                    "balcony": None,
                    "basement": None,
                    "parking": None,
                    "address": None,
                    "floor": None,
                    "furnished": None,
                    "elevator": None,
                    "renovated": None
                }
                item = PikFlat.PikFlat(properties)
                self._items.append(item)
                del item

        logger.debug("Leaving")
        return self._items

    def update_flat_details(self, flat: PikFlat.PikFlat) -> bool:
        """

        :param flat:
        :type flat: instance of PikFlat class
        :return:
        :rtype: bool
        """
        logger.debug("Entered")
        logger.debug("Link to open: {0}".format(flat.get_link()))
        # Get detailed information about item by retrieving the page of this item
        detailed = self._get_page(flat.get_link())

        if detailed is None:
            logger.critical("Unable to parse data because detailed page was not retrieved.")
            logger.debug("Leaving")
            return False

        # Square meters
        start = detailed.find('Kvadrata', 0)
        detailed = detailed[start + 18:]
        start = detailed.find('>')
        end = detailed.find('<', start)
        area = detailed[start + 1:end]
        logger.debug("Converting area string value '{0}' into float value".format(area))
        area = area.replace(",", ".")
        area = area.replace("+", "")
        area = area.lstrip()
        area = area.rstrip()
        try:
            area = float(area)
        except ValueError:
            logger.error("Unable to convert the area string to a float value for item {0}.".format(flat.get_item_id()))
            logger.warning("Setting the area to zero.")
            area = 0
        flat.set_area(area)

        # Number of rooms
        start = detailed.find('soba', 0)
        detailed = detailed[start + 14:]
        start = detailed.find('>')
        end = detailed.find('<', start)
        rooms = detailed[start + 1:end]
        if rooms.find("Jednosoban") != -1:
            rooms = 1
        elif rooms.find("Jednoiposoban") != -1:
            # Must be with "." because if you use "," it will be tuple and not float
            rooms = 1.5
        elif rooms.find("Dvosoban") != -1:
            rooms = 2
        elif rooms.find("Trosoban") != -1:
            rooms = 3
        elif rooms.find("Četverosoban") != -1:
            rooms = 4
        else:
            logger.warning("Unable to determine the number of rooms for item {0}.".format(flat.get_item_id()))
            logger.warning("Setting the number of rooms to zero.")
            rooms = 0
        flat.set_number_of_rooms(rooms)

        # Floor (story)
        start = detailed.find('Sprat', 0)
        if start == -1:
            logger.warning("Unable to find the floor information for item {0}.".format(flat.get_item_id()))
            logger.warning("Setting the floor to -100.")
            # Not setting it to -1 as this is a possible value
            floor = -100
        else:
            detailed = detailed[start + 15:]
            start = detailed.find('>')
            end = detailed.find('<', start)
            floor = detailed[start + 1:end]
            floor = floor.lower()
            if floor.find("prizemlje") != -1:
                logger.debug("Must be ground floor.")
                floor = 0
            else:
                try:
                    floor = int(floor)
                except ValueError:
                    logger.error("Unable to convert the floor string into an integer for item {0}.".format(flat.get_item_id()))
                    logger.warning("Setting the floor to -100")
                    # Not setting it to -1 as this is a possible value
                    floor = -100
        flat.set_floor(floor)

        # Heating
        start = detailed.find('grijanja', 0)
        if start == -1:
            heating = "Unknown"
        else:
            detailed = detailed[start + 18:]
            start = detailed.find('>')
            end = detailed.find('<', start)
            heating = html.unescape(html.unescape(detailed[start + 1:end]))
        flat.set_type_of_heating(heating)

        # Furniture
        start = detailed.find('ten?', 0)
        if start == -1:
            furniture = "Unknown"
        else:
            detailed = detailed[start + 14:]
            start = detailed.find('>')
            end = detailed.find('<', start)
            furniture = html.unescape(html.unescape(detailed[start + 1:end]))
        flat.set_furnished(furniture)

        # Address
        start = detailed.find('Adresa', end)
        if start == -1:
            address = "Unknown"
        else:
            detailed = detailed[start + 16:]
            start = detailed.find('>')
            end = detailed.find('<', start)
            address = html.unescape(html.unescape(detailed[start + 1:end]))
        flat.set_address(address)

        # Built in year
        start = detailed.find('izgradnje', 0)
        if start == -1:
            year = "Unknown"
        else:
            detailed = detailed[start + 19:]
            start = detailed.find('>')
            end = detailed.find('<', start)
            year = detailed[start + 1:end]
        flat.set_built_in_years(year)

        # Following properties are of type bool - if they exist, they are set to true
        # Balcony
        start = detailed.find('Balkon', 0)
        if start != -1:
            balcony = True
        else:
            balcony = False
            start = 0
        flat.set_balcony(balcony)

        # Elevator
        start = detailed.find('Lift', start)
        if start != -1:
            elevator = True
        else:
            elevator = False
            start = 0
        flat.set_elevator(elevator)

        # Recently renovated
        start = detailed.find('adaptiran', start)
        if start != -1:
            renovated = True
        else:
            renovated = False
            start = 0
        flat.set_renovated(renovated)

        # Basement
        start = detailed.find('Ostava', start)
        if start != -1:
            basement = True
        else:
            basement = False
            start = 0
        flat.set_basement(basement)

        # Parking
        start = detailed.find('Parking', start)
        if start != -1:
            parking = True
        else:
            parking = False
            start = 0
        flat.set_parking(parking)

        # Published by
        start = detailed.find('username', start)
        if start == -1:
            publishedBy = "Unknown"
        else:
            temp = detailed.find('>', start+11)
            end = detailed.find('<', temp)
            publishedBy = html.unescape(html.unescape(detailed[temp+1: end]))
        flat.set_published_by(publishedBy)

        logger.debug("Leaving")
        return True

    @staticmethod
    def get_version():
        """

        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return packageVersion
