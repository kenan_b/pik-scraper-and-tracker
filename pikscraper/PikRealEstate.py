#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from . import PikItem
from datetime import date

logger = logging.getLogger(__name__)


class PikRealEstate(PikItem.PikItem):
    """

    """
    def __init__(self, properties: dict):
        """

        :param properties:
        :type properties: dict
        """
        # Detailed info - obtained from the item page
        logger.debug("Entered")
        self._renewed = properties["renewed"]
        self._area = properties["area"]
        self._numberOfRooms = properties["numberOfRooms"]
        self._typeOfHeating = properties["typeOfHeating"]
        self._builtIn = properties["builtIn"]
        self._balcony = properties["balcony"]
        self._basement = properties["basement"]
        self._parking = properties["parking"]
        super().__init__(properties)
        # Must call the super class constructor before calling the set_price_per_meter as the price of the item must be
        # available
        if (self._area != 0) and (self._area is not None):
            self._set_price_per_meter()
        else:
            self._pricePerMeter = 0

        logger.debug("Leaving")

    def __str__(self) -> str:
        """
        Creates string representation of class object.

        This method gets executed whenever PikRealEstate object is sent to the print() method.

        :return: string representation of object.
        :rtype: string
        """
        logger.debug("Entered")
        text = super().__str__()
        text += "Renewed {0}\n".format(self._renewed)
        text += "Area: {0}\n".format(self._area)
        if self._pricePerMeter == 0:
            text += "Price per m^2: Unknown\n"
        else:
            text += "Price per m^2: {0}\n".format(self._pricePerMeter)
        text += "Number of rooms: {0}\n".format(self._numberOfRooms)
        text += "Heating type: {0}\n".format(self._typeOfHeating)
        text += "Built in: {0}\n".format(self._builtIn)
        if self._balcony:
            text += "Balcony: Yes\n"
        else:
            text += "Balcony: No\n"
        if self._basement:
            text += "Basement: Yes\n"
        else:
            text += "Basement: No\n"
        if self._parking:
            text += "Parking: Yes\n"
        else:
            text += "Parking: No\n"

        logger.debug("Leaving")
        return text

    def get_renewed(self) -> str:
        """

        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._renewed

    def set_renewed(self, renewed: date) -> None:
        """

        :param renewed:
        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        self._renewed = renewed

    def get_area(self) -> float:
        """

        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._area

    def set_area(self, area: float) -> None:
        """

        :param area:
        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        self._area = area
        self._set_price_per_meter()

    def get_number_of_rooms(self) -> float:
        """

        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._numberOfRooms

    def set_number_of_rooms(self, rooms: float) -> None:
        """

        :param rooms:
        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        self._numberOfRooms = rooms

    def get_type_of_heating(self) -> str:
        """

        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._typeOfHeating

    def set_type_of_heating(self, heating: str) -> None:
        """

        :param heating:
        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        self._typeOfHeating = heating

    def get_built_in_years(self) -> str:
        """

        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._builtIn

    def set_built_in_years(self, years: str) -> None:
        """

        :param years:
        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        self._builtIn = years

    def get_balcony(self) -> bool:
        """

        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._balcony

    def set_balcony(self, balcony: bool) -> None:
        """

        :param balcony:
        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        self._balcony = balcony

    def get_basement(self) -> bool:
        """

        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._basement

    def set_basement(self, basement: bool) -> None:
        """

        :param basement:
        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        self._basement = basement

    def get_parking(self) -> bool:
        """

        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._parking

    def set_parking(self, parking: bool) -> None:
        """

        :param parking:
        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        self._parking = parking

    def set_price(self, price: float) -> None:
        """
        Sets the price of the Real Estate.

        Sets the price of the real estate by calling the superclass set_price method and also recalculates and sets
        the price per square meter.
        :param price: Real estate price
        :type price: float
        :return: No return value
        """
        logger.debug("Entered")
        super().set_price(price)
        self._set_price_per_meter()
        logger.debug("Leaving")
        return

    def _set_price_per_meter(self) -> None:
        """

        :return:
        """
        logger.debug("Entered")
        if (self._area != 0) and (self._area is not None):
            logger.debug("Area of the flat: {0}".format(self.get_area()))
            if self.get_price() != 0:
                logger.debug("Price of the flat: {0}".format(self.get_price()))
                self._pricePerMeter = self.get_price() / self.get_area()
            else:
                logger.warning("Unable to calculate price per square meter. Flat price is {0}".format(self.get_price()))
                self._pricePerMeter = 0
        else:
            logger.warning("Unable to calculate price per square meter. "
                           "ID {0} - area {1}".format(self.get_item_id(), self._area))
            self._pricePerMeter = 0

        logger.debug("Leaving")
        return

    def get_price_per_meter(self):
        """

        :return:
        """
        logger.debug("Entered")
        logger.debug("Leaving")
        return self._pricePerMeter
